/*
 * Copyright (c) 2022-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "UTTest_dm_auth_manager.h"

#include "nlohmann/json.hpp"

#include "dm_log.h"
#include "dm_constants.h"
#include "dm_auth_manager.h"
#include "auth_message_processor.h"
#include "device_manager_service_listener.h"

namespace OHOS {
namespace DistributedHardware {
void DmAuthManagerTest::SetUp()
{
    authManager_->authMessageProcessor_ = std::make_shared<AuthMessageProcessor>(authManager_);
    authManager_->authMessageProcessor_->authResponseContext_ = std::make_shared<DmAuthResponseContext>();
    authManager_->authRequestContext_ = std::make_shared<DmAuthRequestContext>();
    authManager_->authRequestState_ = std::make_shared<AuthRequestFinishState>();
    authManager_->authResponseContext_ = std::make_shared<DmAuthResponseContext>();
    authManager_->authResponseState_ = std::make_shared<AuthResponseConfirmState>();
    authManager_->hiChainAuthConnector_ = std::make_shared<HiChainAuthConnector>();
    authManager_->softbusConnector_ = std::make_shared<SoftbusConnector>();
    authManager_->softbusConnector_->GetSoftbusSession()->
        RegisterSessionCallback(std::shared_ptr<ISoftbusSessionCallback>(authManager_));
    authManager_->timer_ = std::make_shared<DmTimer>();
}
void DmAuthManagerTest::TearDown()
{
}
void DmAuthManagerTest::SetUpTestCase()
{
}
void DmAuthManagerTest::TearDownTestCase()
{
}

namespace {
const int32_t MIN_PIN_CODE = 100000;
const int32_t MAX_PIN_CODE = 999999;
/**
 * @tc.name: DmAuthManager::UnAuthenticateDevice_001
 * @tc.desc: Call unauthenticateddevice to check whether the return value is DM_ FAILED
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, UnAuthenticateDevice_001, testing::ext::TestSize.Level0)
{
    std::shared_ptr<SoftbusSession> sessionSession = std::shared_ptr<SoftbusSession>();
    std::shared_ptr<DmAuthRequestContext> authRequestContext = std::make_shared<DmAuthRequestContext>();
    std::string pkgName = "";
    std::string deviceId = "222";
    int32_t ret = authManager_->UnAuthenticateDevice(pkgName, deviceId);
    ASSERT_EQ(ret, ERR_DM_FAILED);
}

/**
 * @tc.name: DmAuthManager::HandleAuthenticateTimeout_001
 * @tc.desc: authResponseContext_= nullptr; Call handleauthenticatemeout to check whether return value is ERR_DM_FAILED
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, HandleAuthenticateTimeout_001, testing::ext::TestSize.Level0)
{
    std::string name = "test";
    std::shared_ptr<AuthRequestState> authRequestState = std::make_shared<AuthRequestNetworkState>();
    authManager_->authRequestState_ = std::make_shared<AuthRequestNetworkState>();
    authManager_->authResponseContext_ = nullptr;
    authManager_->SetAuthRequestState(authRequestState);
    authManager_->HandleAuthenticateTimeout(name);
    ASSERT_TRUE(authManager_->authResponseContext_ != nullptr);
}

/**
 * @tc.name: DmAuthManager::HandleAuthenticateTimeout_002
 * @tc.desc: authResponseContext_= nullptr; Call handleauthenticatemeout to check whether the return value is DM_OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, HandleAuthenticateTimeout_002, testing::ext::TestSize.Level0)
{
    std::string name = "test";
    std::shared_ptr<AuthRequestState> authRequestState = std::make_shared<AuthRequestFinishState>();
    authManager_->SetAuthRequestState(authRequestState);
    authManager_->HandleAuthenticateTimeout(name);
    ASSERT_TRUE(authManager_->authRequestState_ != nullptr);
}

/**
 * @tc.name: DmAuthManager::EstablishAuthChannel_001
 * @tc.desc: Call establishauthchannel to check whether the return value is DM_ FAILED
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, EstablishAuthChannel_001, testing::ext::TestSize.Level0)
{
    std::shared_ptr<SoftbusSession> sessionSession = std::shared_ptr<SoftbusSession>();
    std::shared_ptr<DmAuthResponseContext> authRequestContext = std::make_shared<DmAuthResponseContext>();
    std::string deviceId1;
    int32_t ret = authManager_->EstablishAuthChannel(deviceId1);
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::StartAuthProcess_001
 * @tc.desc: Whether the return value of calling startauthprocess is ERR_DM_FAILED
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, StartAuthProcess_001, testing::ext::TestSize.Level0)
{
    std::shared_ptr<AuthResponseState> authResponseState = std::make_shared<AuthResponseConfirmState>();
    authManager_->SetAuthResponseState(authResponseState);
    int32_t action = 0;
    authManager_->StartAuthProcess(action);
    bool ret = authManager_->authRequestContext_->deviceName.empty();
    ASSERT_EQ(ret, true);
}

/**
 * @tc.name: DmAuthManager::StartAuthProcess_002
 * @tc.desc: Whether the return value of calling startauthprocess is DM_ OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, StartAuthProcess_002, testing::ext::TestSize.Level0)
{
    std::shared_ptr<AuthResponseState> authResponseState = std::make_shared<AuthResponseInitState>();
    authManager_->authResponseState_ = std::make_shared<AuthResponseInitState>();
    authManager_->SetAuthResponseState(authResponseState);
    authManager_->authResponseContext_->sessionId = 111;
    authManager_->softbusConnector_->GetSoftbusSession()->RegisterSessionCallback(authManager_);
    int32_t action = 1;
    int32_t ret = authManager_->StartAuthProcess(action);
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::StartAuthProcess_003
 * @tc.desc: Set authResponseContext_ is nullptr return ERR_DM_AUTH_NOT_START
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, StartAuthProcess_003, testing::ext::TestSize.Level0)
{
    authManager_->authResponseContext_ = nullptr;
    int32_t action = 1;
    int32_t ret = authManager_->StartAuthProcess(action);
    ASSERT_EQ(ret, ERR_DM_AUTH_NOT_START);
}

/**
 * @tc.name: DmAuthManager::CreateGroup_001
 * @tc.desc: Whether the return value of calling creategroup is DM_ OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, CreateGroup_001, testing::ext::TestSize.Level0)
{
    std::shared_ptr<AuthResponseState> authResponseState = std::make_shared<AuthResponseConfirmState>();
    std::shared_ptr<HiChainConnector> hiChainConnector = std::make_shared<HiChainConnector>();
    authManager_->SetAuthResponseState(authResponseState);
    authManager_->authResponseContext_->requestId = 111;
    authManager_->authResponseContext_->groupName = "111";
    int32_t ret = authManager_->CreateGroup();
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::CreateGroup_002
 * @tc.desc: Whether the return value of calling creategroup is ERR_DM_FAILED
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, CreateGroup_002, testing::ext::TestSize.Level0)
{
    authManager_->authResponseContext_ = nullptr;
    int32_t ret = authManager_->CreateGroup();
    ASSERT_EQ(ret, ERR_DM_FAILED);
}

/**
 * @tc.name: DmAuthManager::AddMember_001
 * @tc.desc: Whether the return value of calling addmember is DM_ OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, AddMember_001, testing::ext::TestSize.Level0)
{
    std::shared_ptr<AuthResponseState> authResponseState = std::make_shared<AuthResponseInitState>();
    std::shared_ptr<HiChainConnector> hiChainConnector = std::make_shared<HiChainConnector>();
    nlohmann::json jsonObject;
    authManager_->authResponseContext_->groupId = "111";
    authManager_->authResponseContext_->groupName = "222";
    authManager_->authResponseContext_->code = 123;
    authManager_->authResponseContext_->requestId = 234;
    authManager_->authResponseContext_->deviceId = "234";
    int32_t pinCode = 444444;
    authManager_->hiChainConnector_->RegisterHiChainCallback(authManager_);
    authManager_->SetAuthResponseState(authResponseState);
    int32_t ret = authManager_->AddMember(pinCode);
    ASSERT_NE(ret, -1);
}

/**
 * @tc.name: DmAuthManager::AddMember_002
 * @tc.desc: Whether the return value of calling  AddMember is ERR_DM_FAILED
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, AddMember_002, testing::ext::TestSize.Level0)
{
    int32_t pinCode = 33333;
    authManager_->authResponseContext_ = nullptr;
    int32_t ret = authManager_->AddMember(pinCode);
    ASSERT_EQ(ret, ERR_DM_FAILED);
}

/**
 * @tc.name: DmAuthManager::JoinNetwork_001
 * @tc.desc: Whether the return value of calling joinnetwork is DM_OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, JoinNetwork_001, testing::ext::TestSize.Level0)
{
    std::shared_ptr<AuthRequestState> authRequestState = std::make_shared<AuthRequestFinishState>();
    const int32_t sessionId = 65;
    const std::string message = "messageTest";
    int64_t requestId = 444;
    const std::string groupId = "{}";
    int32_t status = 1;
    authManager_->OnGroupCreated(requestId, groupId);
    authManager_->OnMemberJoin(requestId, status);
    authManager_->OnDataReceived(sessionId, message);
    authManager_->SetAuthRequestState(authRequestState);
    int32_t ret = authManager_->JoinNetwork();
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::JoinNetwork_002
 * @tc.desc: Whether the return value of calling joinnetwork is ERR_DM_FAILED
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, JoinNetwork_002, testing::ext::TestSize.Level0)
{
    authManager_->authResponseContext_ = nullptr;
    int32_t userId = 1;
    authManager_->UserSwitchEventCallback(userId);
    authManager_->AuthenticateFinish();
    authManager_->CancelDisplay();
    int32_t ret = authManager_->JoinNetwork();
    ASSERT_EQ(ret, ERR_DM_FAILED);
}

/**
 * @tc.name: DmAuthManager::SetAuthResponseState_001
 * @tc.desc: Is the authresponsestate assignment successful
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, SetAuthResponseState_001, testing::ext::TestSize.Level0)
{
    std::shared_ptr<AuthResponseState> authResponseState = std::make_shared<AuthResponseFinishState>();
    authManager_->authResponseState_ = std::make_shared<AuthResponseFinishState>();
    authManager_->SetAuthResponseState(authResponseState);
    int32_t ret = authManager_->SetAuthResponseState(authResponseState);
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::GetPinCode_001
 * @tc.desc: Return authresponsecontext - > code
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, GetPinCode_001, testing::ext::TestSize.Level0)
{
    authManager_->authResponseContext_->code = 123456;
    int32_t ret = authManager_->GetPinCode();
    ASSERT_EQ(ret, 123456);
}

/**
 * @tc.name: DmAuthManager::GetPinCode_002
 * @tc.desc: Set authResponseContext_ is nullptr Return ERR_DM_AUTH_NOT_START
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, GetPinCode_002, testing::ext::TestSize.Level0)
{
    authManager_->authResponseContext_ = nullptr;
    authManager_->ShowConfigDialog();
    authManager_->ShowAuthInfoDialog();
    authManager_->ShowStartAuthDialog();
    int32_t ret = authManager_->GetPinCode();
    ASSERT_EQ(ret, ERR_DM_AUTH_NOT_START);
}

/**
 * @tc.name: DmAuthManager::SetPageId_001
 * @tc.desc: Return DM_OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, SetPageId_001, testing::ext::TestSize.Level0)
{
    int32_t pageId = 123;
    int32_t ret = authManager_->SetPageId(pageId);
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::SetPageId_002
 * @tc.desc: Return ERR_DM_AUTH_NOT_START
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, SetPageId_002, testing::ext::TestSize.Level0)
{
    int32_t pageId = 123;
    authManager_->authResponseContext_ = nullptr;
    authManager_->authMessageProcessor_ = nullptr;
    const int32_t sessionId = 65;
    const std::string message = "messageTest";
    int64_t requestId = 555;
    int32_t status = 2;
    authManager_->OnMemberJoin(requestId, status);
    authManager_->OnDataReceived(sessionId, message);
    int32_t ret = authManager_->SetPageId(pageId);
    ASSERT_EQ(ret, ERR_DM_AUTH_NOT_START);
}

/**
 * @tc.name: DmAuthManager::SetReasonAndFinish_001
 * @tc.desc: Return ERR_DM_AUTH_NOT_START
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, SetReasonAndFinish_001, testing::ext::TestSize.Level0)
{
    const int32_t sessionId = 78;
    int32_t reason = 123;
    int32_t state = 456;
    authManager_->OnSessionClosed(sessionId);
    authManager_->authResponseContext_ = nullptr;
    int64_t requestId = 333;
    const std::string groupId = "{}";
    authManager_->OnGroupCreated(requestId, groupId);
    int32_t ret = authManager_->SetReasonAndFinish(reason, state);
    ASSERT_EQ(ret, ERR_DM_AUTH_NOT_START);
}

/**
 * @tc.name: DmAuthManager::SetReasonAndFinish_002
 * @tc.desc: Return DM_OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, SetReasonAndFinish_002, testing::ext::TestSize.Level0)
{
    int32_t reason = 1234;
    int32_t state = 5678;
    int64_t requestId = 22;
    const std::string groupId = "{}";
    authManager_->OnGroupCreated(requestId, groupId);
    int32_t ret = authManager_->SetReasonAndFinish(reason, state);
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::SetReasonAndFinish_003
 * @tc.desc: Return DM_OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, SetReasonAndFinish_003, testing::ext::TestSize.Level0)
{
    authManager_->authRequestState_ = nullptr;
    authManager_->authResponseState_ = std::make_shared<AuthResponseFinishState>();
    int32_t reason = 12;
    int32_t state = 36;
    int32_t ret = authManager_->SetReasonAndFinish(reason, state);
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::IsIdenticalAccount_001
 * @tc.desc: Return false
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, IsIdenticalAccount_001, testing::ext::TestSize.Level0)
{
    bool ret = authManager_->IsIdenticalAccount();
    ASSERT_EQ(ret, false);
}

/**
 * @tc.name: DmAuthManager::GeneratePincode_001
 * @tc.desc: Return OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, GeneratePincode_001, testing::ext::TestSize.Level0)
{
    int32_t openedSessionId = 66;
    int32_t sessionSide = 0;
    int32_t result = 3;
    const int32_t closedSessionId = 11;
    authManager_->OnSessionOpened(openedSessionId, sessionSide, result);
    authManager_->OnSessionClosed(closedSessionId);
    int32_t ret = authManager_->GeneratePincode();
    ASSERT_LE(ret, MAX_PIN_CODE);
    ASSERT_GE(ret, MIN_PIN_CODE);
}

/**
 * @tc.name: DmAuthManager::AuthenticateDevice_001
 * @tc.desc: Return ERR_DM_AUTH_FAILED
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, AuthenticateDevice_001, testing::ext::TestSize.Level0)
{
    std::string pkgName = "com.ohos.test";
    int32_t authType = -1;
    std::string deviceId = "113456";
    std::string extra = "extraTest";
    int32_t ret = authManager_->AuthenticateDevice(pkgName, authType, deviceId, extra);
    ASSERT_EQ(ret, ERR_DM_AUTH_FAILED);
}

/**
 * @tc.name: DmAuthManager::AuthenticateDevice_002
 * @tc.desc: Return ERR_DM_INPUT_PARA_INVALID
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, AuthenticateDevice_002, testing::ext::TestSize.Level0)
{
    std::string pkgName = "";
    int32_t authType = 1;
    std::string deviceId = "113456";
    std::string extra = "extraTest";
    int32_t ret = authManager_->AuthenticateDevice(pkgName, authType, deviceId, extra);
    ASSERT_EQ(ret, ERR_DM_INPUT_PARA_INVALID);
}

/**
 * @tc.name: DmAuthManager::AuthenticateDevice_003
 * @tc.desc: Return ERR_DM_INPUT_PARA_INVALID
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, AuthenticateDevice_003, testing::ext::TestSize.Level0)
{
    std::string pkgName = "com.ohos.test";
    int32_t authType = 1;
    std::string deviceId = "234568";
    std::string extra = "extraTest";
    authManager_->listener_ = nullptr;
    int32_t ret = authManager_->AuthenticateDevice(pkgName, authType, deviceId, extra);
    ASSERT_EQ(ret, ERR_DM_INPUT_PARA_INVALID);
}

/**
 * @tc.name: DmAuthManager::AuthenticateDevice_004
 * @tc.desc: Return ERR_DM_UNSUPPORTED_AUTH_TYPE
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, AuthenticateDevice_004, testing::ext::TestSize.Level0)
{
    std::string pkgName = "com.ohos.test";
    int32_t authType = 4;
    std::string deviceId = "deviceIdTest";
    std::string extra = "extraTest";
    int32_t ret = authManager_->AuthenticateDevice(pkgName, authType, deviceId, extra);
    ASSERT_EQ(ret, ERR_DM_UNSUPPORTED_AUTH_TYPE);
}

/**
 * @tc.name: DmAuthManager::AuthenticateDevice_005
 * @tc.desc: Return ERR_DM_AUTH_BUSINESS_BUSY
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, AuthenticateDevice_005, testing::ext::TestSize.Level0)
{
    std::string pkgName = "com.ohos.test";
    int32_t authType = 1;
    std::string deviceId = "deviceIdTest";
    std::string extra = "extraTest";
    authManager_->authRequestState_ = std::make_shared<AuthRequestNetworkState>();
    int32_t ret = authManager_->AuthenticateDevice(pkgName, authType, deviceId, extra);
    ASSERT_EQ(ret, ERR_DM_AUTH_BUSINESS_BUSY);
}

/**
 * @tc.name: DmAuthManager::AuthenticateDevice_006
 * @tc.desc: Return ERR_DM_INPUT_PARA_INVALID
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, AuthenticateDevice_006, testing::ext::TestSize.Level0)
{
    std::string pkgName = "com.ohos.test";
    int32_t authType = 1;
    std::string deviceId = "deviceIdTest";
    std::string extra = "extraTest";
    const int32_t sessionId = 1;
    authManager_->authRequestState_ = nullptr;
    authManager_->authResponseState_ = nullptr;
    authManager_->authResponseContext_ = nullptr;
    authManager_->StartNegotiate(sessionId);
    authManager_->RespNegotiate(sessionId);
    authManager_->SendAuthRequest(sessionId);
    authManager_->StartRespAuthProcess();
    int32_t ret = authManager_->AuthenticateDevice(pkgName, authType, deviceId, extra);
    ASSERT_EQ(ret, ERR_DM_INPUT_PARA_INVALID);
}

/**
 * @tc.name: DmAuthManager::AuthenticateDevice_007
 * @tc.desc: Return DM_OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, AuthenticateDevice_007, testing::ext::TestSize.Level0)
{
    std::string pkgName = "com.ohos.test";
    int32_t authType = 1;
    std::string deviceId = "deviceIdTest";
    std::string extra = "extraTest";
    std::shared_ptr<DeviceInfo> infoPtr = std::make_shared<DeviceInfo>();
    authManager_->authRequestState_ = nullptr;
    authManager_->authResponseState_ = nullptr;
    authManager_->timer_ = nullptr;
    authManager_->softbusConnector_->discoveryDeviceInfoMap_.emplace(deviceId, infoPtr);
    int32_t ret = authManager_->AuthenticateDevice(pkgName, authType, deviceId, extra);
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::GenerateGroupName_001
 * @tc.desc: Return DM_OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, GenerateGroupName_001, testing::ext::TestSize.Level0)
{
    authManager_->authResponseContext_ = nullptr;
    std::string ret = authManager_->GenerateGroupName();
    ASSERT_TRUE(ret.empty());
}

/**
 * @tc.name: DmAuthManager::GenerateGroupName_002
 * @tc.desc: Return DM_OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, GenerateGroupName_002, testing::ext::TestSize.Level0)
{
    authManager_->authResponseContext_->targetPkgName = "targetPkgNameTest";
    authManager_->authResponseContext_->localDeviceId = "localDeviceIdTest";
    std::string ret = authManager_->GenerateGroupName();
    ASSERT_TRUE(!ret.empty());
}

/**
 * @tc.name: DmAuthManager::GetIsCryptoSupport_001
 * @tc.desc: Set authResponseState_ is nullptr Return false
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, GetIsCryptoSupport_001, testing::ext::TestSize.Level0)
{
    authManager_->authResponseState_ = nullptr;
    bool ret = authManager_->GetIsCryptoSupport();
    ASSERT_EQ(ret, false);
}

/**
 * @tc.name: DmAuthManager::GetIsCryptoSupport_002
 * @tc.desc: Set authResponseState_ is not nullptr and authRequestState_ is nullptr Return false
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, GetIsCryptoSupport_002, testing::ext::TestSize.Level0)
{
    authManager_->authResponseState_ = std::make_shared<AuthResponseNegotiateState>();
    authManager_->authRequestState_ = nullptr;
    bool ret = authManager_->GetIsCryptoSupport();
    ASSERT_EQ(ret, false);
}

/**
 * @tc.name: DmAuthManager::GetIsCryptoSupport_003
 * @tc.desc: Set authResponseState_ is not nullptr and authRequestState_ is nullptr Return false
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, GetIsCryptoSupport_003, testing::ext::TestSize.Level0)
{
    authManager_->authResponseState_ = std::make_shared<AuthResponseNegotiateState>();
    authManager_->authRequestState_ = std::make_shared<AuthRequestNegotiateState>();
    bool ret = authManager_->GetIsCryptoSupport();
    ASSERT_EQ(ret, false);
}

/**
 * @tc.name: DmAuthManager::OnUserOperation_001
 * @tc.desc: Set authResponseContext_ is nullptr Return ERR_DM_AUTH_NOT_START
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, OnUserOperation_001, testing::ext::TestSize.Level0)
{
    int32_t action = 0;
    std::string params = "paramsTest";
    authManager_->authResponseContext_ = nullptr;
    int32_t ret = authManager_->OnUserOperation(action, params);
    ASSERT_EQ(ret, ERR_DM_AUTH_NOT_START);
}

/**
 * @tc.name: DmAuthManager::OnUserOperation_002
 * @tc.desc: Set authResponseContext_ is not nullptr Return DM_OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, OnUserOperation_002, testing::ext::TestSize.Level0)
{
    int32_t action = 1;
    std::string params = "paramsTest1";
    int32_t ret = authManager_->OnUserOperation(action, params);
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::OnUserOperation_003
 * @tc.desc: Set authResponseContext_ is not nullptr Return DM_OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, OnUserOperation_003, testing::ext::TestSize.Level0)
{
    int32_t action = 2;
    std::string params = "paramsTest2";
    int32_t ret = authManager_->OnUserOperation(action, params);
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::OnUserOperation_004
 * @tc.desc: Set authResponseContext_ is not nullptr Return DM_OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, OnUserOperation_004, testing::ext::TestSize.Level0)
{
    int32_t action = 3;
    std::string params = "paramsTest3";
    int32_t ret = authManager_->OnUserOperation(action, params);
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::OnUserOperation_005
 * @tc.desc: Set authResponseContext_ is not nullptr Return DM_OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, OnUserOperation_005, testing::ext::TestSize.Level0)
{
    int32_t action = 4;
    std::string params = "paramsTest4";
    int32_t ret = authManager_->OnUserOperation(action, params);
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::OnUserOperation_006
 * @tc.desc: Set authResponseContext_ is not nullptr Return DM_OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, OnUserOperation_006, testing::ext::TestSize.Level0)
{
    int32_t action = 5;
    std::string params = "5";
    int32_t ret = authManager_->OnUserOperation(action, params);
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::OnUserOperation_007
 * @tc.desc: Set authResponseContext_ is not nullptr Return DM_OK
 * @tc.type: FUNC
 * @tc.require: AR000GHSJK
 */
HWTEST_F(DmAuthManagerTest, OnUserOperation_007, testing::ext::TestSize.Level0)
{
    int32_t action = 1111;
    std::string params = "paramsTest1111";
    int32_t ret = authManager_->OnUserOperation(action, params);
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::UnBindDevice001
 * @tc.desc: Set pkgName not null
 *           Set udidHash not null
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, UnBindDevice001, testing::ext::TestSize.Level0)
{
    std::string pkgName = "com.ohos.test";
    std::string udidHash = "udidHash";
    int32_t ret = authManager_->UnBindDevice(pkgName, udidHash);
    ASSERT_EQ(ret, ERR_DM_FAILED);
}

/**
 * @tc.name: DmAuthManager::UnBindDevice002
 * @tc.desc: Set pkgName not null
 *           Set udidHash not null
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, UnBindDevice002, testing::ext::TestSize.Level0)
{
    std::string pkgName;
    std::string udidHash = "udidHash";
    int32_t ret = authManager_->UnBindDevice(pkgName, udidHash);
    ASSERT_EQ(ret, ERR_DM_FAILED);
}

/**
 * @tc.name: DmAuthManager::RequestCredential001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, RequestCredential001, testing::ext::TestSize.Level0)
{
    authManager_->hiChainAuthConnector_ = std::make_shared<HiChainAuthConnector>();
    authManager_->RequestCredential();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::GenerateCredential001
 * @tc.desc: Set publicKey not null
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, GenerateCredential001, testing::ext::TestSize.Level0)
{
    std::string publicKey = "publicKey";
    authManager_->GenerateCredential(publicKey);
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::RequestCredentialDone001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, RequestCredentialDone001, testing::ext::TestSize.Level0)
{
    authManager_->RequestCredentialDone();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::RequestCredentialDone002
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, RequestCredentialDone002, testing::ext::TestSize.Level0)
{
    authManager_->hiChainAuthConnector_ = std::make_shared<HiChainAuthConnector>();
    authManager_->RequestCredential();
    authManager_->authRequestState_ = std::make_shared<AuthRequestNegotiateState>();
    authManager_->RequestCredentialDone();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::ImportCredential001
 * @tc.desc: Set deviceId not null
 *           Set publicKey not null
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, ImportCredential001, testing::ext::TestSize.Level0)
{
    std::string deviceId = "deviceId";
    std::string publicKey = "publicKey";
    int32_t ret = authManager_->ImportCredential(deviceId, publicKey);
    ASSERT_NE(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::EstablishUnbindChannel001
 * @tc.desc: Set deviceIdHash not null
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, EstablishUnbindChannel001, testing::ext::TestSize.Level0)
{
    std::string deviceIdHash = "deviceIdHash";
    int32_t ret = authManager_->EstablishUnbindChannel(deviceIdHash);
    ASSERT_EQ(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::RequestSyncDeleteAcl001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, RequestSyncDeleteAcl001, testing::ext::TestSize.Level0)
{
    authManager_->RequestSyncDeleteAcl();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::RequestSyncDeleteAcl002
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, RequestSyncDeleteAcl002, testing::ext::TestSize.Level0)
{
    authManager_->timer_ = nullptr;
    authManager_->RequestSyncDeleteAcl();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::SrcSyncDeleteAclDonel001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, SrcSyncDeleteAclDonel001, testing::ext::TestSize.Level0)
{
    authManager_->isFinishOfLocal_ = true;
    authManager_->SrcSyncDeleteAclDone();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::SrcSyncDeleteAclDonel002
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, SrcSyncDeleteAclDonel002, testing::ext::TestSize.Level0)
{
    authManager_->isFinishOfLocal_ = false;
    authManager_->SrcSyncDeleteAclDone();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::SinkSyncDeleteAclDone001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, SinkSyncDeleteAclDone001, testing::ext::TestSize.Level0)
{
    authManager_->isFinishOfLocal_ = true;
    authManager_->SinkSyncDeleteAclDone();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::SinkSyncDeleteAclDone002
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, SinkSyncDeleteAclDone002, testing::ext::TestSize.Level0)
{
    authManager_->isFinishOfLocal_ = false;
    authManager_->SinkSyncDeleteAclDone();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::SyncDeleteAclDone001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, SyncDeleteAclDone001, testing::ext::TestSize.Level0)
{
    authManager_->SyncDeleteAclDone();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::SyncDeleteAclDone002
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, SyncDeleteAclDone002, testing::ext::TestSize.Level0)
{
    authManager_->authRequestState_ = nullptr;
    authManager_->SyncDeleteAclDone();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::ResponseCredential001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, ResponseCredential001, testing::ext::TestSize.Level0)
{
    authManager_->ResponseCredential();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::ResponseSyncDeleteAcl001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, ResponseSyncDeleteAcl001, testing::ext::TestSize.Level0)
{
    authManager_->ResponseSyncDeleteAcl();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::AuthDeviceTransmit001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, AuthDeviceTransmit001, testing::ext::TestSize.Level0)
{
    int64_t requestId = 0;
    uint8_t *data = nullptr;
    uint32_t dataLen = 0;
    bool ret = authManager_->AuthDeviceTransmit(requestId, data, dataLen);
    ASSERT_EQ(ret, false);
}

/**
 * @tc.name: DmAuthManager::SrcAuthDeviceFinish001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, SrcAuthDeviceFinish001, testing::ext::TestSize.Level0)
{
    authManager_->authRequestState_ = std::make_shared<AuthRequestNegotiateState>();
    authManager_->SrcAuthDeviceFinish();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::SinkAuthDeviceFinish001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, SinkAuthDeviceFinish001, testing::ext::TestSize.Level0)
{
    authManager_->authRequestState_ = std::make_shared<AuthRequestNegotiateState>();
    authManager_->SinkAuthDeviceFinish();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::AuthDeviceFinish001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, AuthDeviceFinish001, testing::ext::TestSize.Level0)
{
    authManager_->authRequestState_ = std::make_shared<AuthRequestNegotiateState>();
    int64_t requestId = 0;
    authManager_->AuthDeviceFinish(requestId);
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::AuthDeviceError001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, AuthDeviceError001, testing::ext::TestSize.Level0)
{
    authManager_->authRequestState_ = std::make_shared<AuthRequestNegotiateState>();
    int64_t requestId = 0;
    int32_t errorCode = -1;
    authManager_->AuthDeviceError(requestId, errorCode);
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::AuthDeviceError002
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, AuthDeviceError002, testing::ext::TestSize.Level0)
{
    authManager_->authRequestState_ = nullptr;
    int64_t requestId = 0;
    int32_t errorCode = -1;
    authManager_->AuthDeviceError(requestId, errorCode);
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::AuthDeviceSessionKey001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, AuthDeviceSessionKey001, testing::ext::TestSize.Level0)
{
    authManager_->authRequestState_ = std::make_shared<AuthRequestNegotiateState>();
    int64_t requestId = 0;
    uint8_t *sessionKey = nullptr;
    uint32_t sessionKeyLen = 0;
    authManager_->AuthDeviceSessionKey(requestId, sessionKey, sessionKeyLen);
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::GetRemoteDeviceId001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, GetRemoteDeviceId001, testing::ext::TestSize.Level0)
{
    std::string deviceId;
    authManager_->GetRemoteDeviceId(deviceId);
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::CompatiblePutAcl001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, CompatiblePutAcl001, testing::ext::TestSize.Level0)
{
    authManager_->authRequestState_ = std::make_shared<AuthRequestNegotiateState>();
    authManager_->CompatiblePutAcl();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::CommonEventCallback001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, CommonEventCallback001, testing::ext::TestSize.Level0)
{
    authManager_->authRequestState_ = std::make_shared<AuthRequestNegotiateState>();
    int32_t userId = 0;
    authManager_->CommonEventCallback(userId);
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::AccountIdLogoutEventCallback001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, AccountIdLogoutEventCallback001, testing::ext::TestSize.Level0)
{
    int32_t userId = 0;
    authManager_->AccountIdLogoutEventCallback(userId);
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::UserSwitchEventCallback001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, UserSwitchEventCallback001, testing::ext::TestSize.Level0)
{
    int32_t userId = 0;
    authManager_->UserSwitchEventCallback(userId);
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::UserChangeEventCallback001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, UserChangeEventCallback001, testing::ext::TestSize.Level0)
{
    int32_t userId = 0;
    authManager_->UserChangeEventCallback(userId);
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::HandleSyncDeleteTimeout001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, HandleSyncDeleteTimeout001, testing::ext::TestSize.Level0)
{
    std::string name;
    authManager_->HandleSyncDeleteTimeout(name);
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::DeleteAcl001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, DeleteAcl001, testing::ext::TestSize.Level0)
{
    std::string pkgName;
    std::string deviceId;
    int32_t ret = authManager_->DeleteAcl(pkgName, deviceId);
    ASSERT_NE(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::ProRespNegotiateExt001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, ProRespNegotiateExt001, testing::ext::TestSize.Level0)
{
    int32_t sessionId = 0;
    authManager_->ProRespNegotiateExt(sessionId);
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::ProRespNegotiate001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, ProRespNegotiate001, testing::ext::TestSize.Level0)
{
    int32_t sessionId = 0;
    authManager_->ProRespNegotiate(sessionId);
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::GenerateBindResultContent001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, GenerateBindResultContent001, testing::ext::TestSize.Level0)
{
    auto ret = authManager_->GenerateBindResultContent();
    ASSERT_EQ(ret.empty(), false);
}

/**
 * @tc.name: DmAuthManager::OnAuthDeviceDataReceived001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, OnAuthDeviceDataReceived001, testing::ext::TestSize.Level0)
{
    int32_t sessionId = 0;
    std::string message;
    authManager_->OnAuthDeviceDataReceived(sessionId, message);
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::OnUnbindSessionOpened001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, OnUnbindSessionOpened001, testing::ext::TestSize.Level0)
{
    int32_t sessionId = 1;
    std::string name = "nameTest";
    std::string networkId = "159753681387291";
    std::string pkgName = "com.ohos.test";
    PeerSocketInfo info = {
        .name = const_cast<char*>(name.c_str()),
        .pkgName = const_cast<char*>(pkgName.c_str()),
        .networkId = const_cast<char*>(networkId.c_str()),
    };
    authManager_->authResponseState_ = nullptr;
    authManager_->authRequestState_ = nullptr;
    authManager_->OnUnbindSessionOpened(sessionId, info);
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::DeleteGroup001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, DeleteGroup001, testing::ext::TestSize.Level0)
{
    std::string pkgName;
    std::string deviceId;
    int32_t ret = authManager_->DeleteGroup(pkgName, deviceId);
    ASSERT_EQ(ret, ERR_DM_FAILED);
}

/**
 * @tc.name: DmAuthManager::DeleteGroup002
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, DeleteGroup002, testing::ext::TestSize.Level0)
{
    std::string pkgName = "pkgName";
    std::string deviceId;
    int32_t ret = authManager_->DeleteGroup(pkgName, deviceId);
    ASSERT_NE(ret, DM_OK);
}

/**
 * @tc.name: DmAuthManager::PutAccessControlList001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, PutAccessControlList001, testing::ext::TestSize.Level0)
{
    authManager_->PutAccessControlList();
    ASSERT_EQ(authManager_->isAuthDevice_, false);
}

/**
 * @tc.name: DmAuthManager::BindSocketFail_001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, BindSocketFail_001, testing::ext::TestSize.Level0)
{
    authManager_->BindSocketFail();
    ASSERT_EQ(authManager_->isFinishOfLocal_, false);
}

/**
 * @tc.name: DmAuthManager::BindSocketSuccess_001
 * @tc.type: FUNC
 */
HWTEST_F(DmAuthManagerTest, BindSocketSuccess_001, testing::ext::TestSize.Level0)
{
    int32_t socket = 1;
    authManager_->authResponseState_ = nullptr;
    authManager_->authRequestState_ = std::make_shared<AuthRequestDeleteInit>();
    authManager_->BindSocketSuccess(socket);
    ASSERT_EQ(authManager_->authRequestContext_->sessionId, socket);
}
} // namespace
} // namespace DistributedHardware
} // namespace OHOS
